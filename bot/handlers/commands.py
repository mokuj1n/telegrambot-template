from aiogram import types
from bot.misc import dp


@dp.message_handler(commands=['start'])
async def start(msg: types.Message):
    await msg.answer("Hello there!")


@dp.message_handler(commands=['cancel'], state='*')
async def cancel(msg: types.Message, state):
    await state.finish()
    await msg.answer(("Canceled"), reply_markup=types.ReplyKeyboardRemove())
