from aiohttp import BasicAuth

import os


BASE_DIR = os.path.dirname(os.path.abspath(__name__))

# BOT SETTINGS
TOKEN = ''
SKIP_UPDATES = True

# WEBHOOK SETTINGS
# TODO: Add webhook settings

# PROXY SETTINGS
PROXY_URL = ''
PROXY_LOGIN = ''
PROXY_PASSWORD = ''
PROXY_AUTH = BasicAuth(login=PROXY_LOGIN, password=PROXY_PASSWORD)

# DATABASE
DB_URL = 'sqlite:///db.sqlite3'

# LOGGING
LOGFILE_PATH = ''
